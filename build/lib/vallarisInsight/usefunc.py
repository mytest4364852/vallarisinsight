from opensearchpy import OpenSearch
import json
import os

def check_file(file_id):
    # ตรวจสอบว่าไฟล์มีอยู่หรือไม่
    if os.path.exists(file_id):
        print(f"The file with ID '{file_id}' exists.")
        return True
    else:
        print(f"The file with ID '{file_id}' does not exist.")
        return False

def remove_host_key_from_env():
    # อ่านไฟล์ .env และลบ key host ออก
    with open(".env", "r") as file:
        lines = file.readlines()

    with open(".env", "w") as file:
        for line in lines:
            if "host" not in line:
                file.write(line)

def login_vallarisInsight(host, port, user, password):
    
    try:
        # เชื่อมต่อ OpenSearch
        client = OpenSearch(
            hosts=[{"host": str(host), "port": int(port)}],
            http_auth=(user, password),
            use_ssl=True,
            verify_certs=False,
            ssl_assert_hostname=False,
            ssl_show_warn=False,
        )

        login = client.info()
        print("Connection to OpenSearch successful.")

        # create folder config
        if not os.path.exists('config_insight'):
            # If not, create it
            os.makedirs('config_insight')

        file_id = "config_insight/.env"

        if os.path.exists(file_id):
            pass
        else:
            pass
        
        data = f"host_insight={str(host)}\n" + f"port_insight={int(port)}\n" + f"user_insight={user}\n" + f"password_insight={password}\n" 
        with open(file_id, "w") as f:
            f.write(data)
            
        return "Login successful"
        

    except Exception as e :
        print(e)
        print("Connection to OpenSearch failed.")
        
        try:
            os.remove("config_insight/.env")
            print("Login failed")
            return "Login failed"
        except FileNotFoundError:
            pass
            return "Login failed"
        except Exception as e:
            pass
            return "Login failed"


def logout_vallarisInsight():
    # ลบไฟล์ .env เมื่อผู้ใช้ logout
    try:
        os.remove("config_insight/.env")
        print("Logout successful.")
        return "Logout successful"
    except FileNotFoundError:
        pass
        return "Logout failed"
    except Exception as e:
        pass
        return "Logout failed"

