from opensearchpy import OpenSearch
import h3
from shapely.geometry import Polygon,Point
import os
import pandas as pd
import geopandas as gpd
import math
from dotenv import load_dotenv

# โหลดตัวแปรสภาพแวดล้อมจากไฟล์ .env ที่ระบุเส้นทาง
env_path = 'config_insight/.env'
load_dotenv(env_path)


def create_open_search_body(minx, miny, maxx, maxy, resolution, options=None, geometry=False):
    
    # สร้างโครงสร้างของคำสั่งค้นหาใน OpenSearch
    body = {
        "aggregations": {
            "filter_agg": {
                "aggregations": {
                    "1": {
                        "aggs": {},
                        "geohex_grid": {
                            "field": "location",
                            "precision": resolution
                        }
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "geo_bounding_box": {
                                    "location": {
                                        "bottom_right": {"lat": miny, "lon": maxx},
                                        "top_left": {"lat": maxy, "lon": minx}
                                    }
                                }
                            },
                            {
                                "range": {
                                    "datadatetime": {
                                        "gte": "0001-01-01T00:00:00Z",
                                        "lte": "2024-08-28T04:46:46.423753Z"
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        },
        "size": 0
    }


    # สร้างส่วน aggregation ของคำสั่งค้นหาจาก options
    if options:
        for option in options:
            body['aggregations']['filter_agg']['aggregations']["1"]["aggs"][f"{option}:majority"] = {
                "terms": {"field": f"{option}.keyword"}
            }

    # เพิ่มส่วนนี้เพื่อกรณีที่ไม่มี options
    else:
     
        # Include all columns (lat, lon, and hexId)
        for key in body:  # เพิ่มคอลัมน์ที่ต้องการทั้งหมด
            body['aggregations']['filter_agg']['aggregations']["1"]["aggs"][f"{key}:majority"] = {
                "terms": {"field": f"{key}.keyword"}
            }

    # print(body)
    return body


def extract_data(response, options, geometry=False):
    # ตรวจสอบว่ามีการตอบกลับ (response) หรือไม่
    if not response:
        return "No response"
    
    # กรองข้อมูลสำหรับ H3 index ที่ระบุ
    data = response['aggregations']['filter_agg']["1"]["buckets"]

     # หากไม่มีข้อมูล
    if not data:
        print("No data available.")
        return "This data is not available "

    # เตรียมข้อมูลสำหรับการคืนค่า
    result = []

    # ถ้ามี options ถูกส่งมา
    if options:
        for bucket in data:
            item = {f"{key}": bucket.get(f"{key}:majority", {}).get('buckets', [{}])[0].get('key') for key in options}
            item['doc_count'] = bucket['doc_count']

            # เพิ่มส่วนนี้สำหรับ geometry
            if geometry:
                item['h3_id'] = bucket['key']
                # print(bucket['key'])
                # print(item['geometry'])

            result.append(item)

    else:
        # ถ้าไม่มี options ถูกส่งมา
        for bucket in data:
            item = {sub_key: sub_value for sub_key, sub_value in bucket.items() if sub_key not in ['key', 'doc_count']}
            item['doc_count'] = bucket['doc_count']

            # เพิ่มส่วนนี้สำหรับ geometry
            if geometry:
                item['h3_id'] = bucket['key']
                # print(item['geometry'])

            result.append(item)

    # print (5555555,result)
    return result

    

def getAllProperties(host_input, port_input ,index_name):
    try:

        host_input = os.getenv("host_insight")
        port_input = os.getenv("port_insight")
        user_input = os.getenv("user_insight")
        password_input = os.getenv("password_insight")


        # เชื่อมต่อ OpenSearch
        client = OpenSearch(
            # hosts=[{"host": str(host) , "port": int(port)}],
            hosts=[{"host": str(host_input), "port": int(port_input)}],
            http_auth=(user_input, password_input),
            use_ssl=True,
            verify_certs=False,
            ssl_assert_hostname=False,
            ssl_show_warn=False,
        )

        # ดึงข้อมูล mapping หรือ properties ของ index ที่ระบุ
        index_mapping = client.indices.get_mapping(index=index_name)

        # Extract and print properties
        properties = index_mapping[index_name]["mappings"]["properties"]
       
        # return arr  # ส่งกลับเป็น JSON string
        arr = [field_name for field_name in properties.keys() if field_name not in ['wkt', 'datadatetime', 'location']]
        return arr

    except Exception as e:
        print(f"Error: {e}")
        print(f"Index '{index_name}' not found or error retrieving properties.")
        return None




def getmain(host_input, port_input, index_name, minx, miny, maxx, maxy, resolution, options=None,geometry=False):

   
    host_input = os.getenv("host_insight")
    port_input = os.getenv("port_insight")
    user_input = os.getenv("user_insight")
    password_input = os.getenv("password_insight")

    
    # เชื่อมต่อ OpenSearch
    client = OpenSearch(
        hosts=[{"host":  str(host_input), "port": int(port_input)}],
        http_auth=(user_input, password_input),
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    if options is None:
        options = getAllProperties(host_input, port_input, index_name)

    else :
        pass

    try:
       
        # miny, minx, maxy, maxx

        # สร้างคำสั่งค้นหาใน OpenSearch
        body = create_open_search_body(minx, miny, maxx, maxy, resolution, options)
        
        # ดำเนินการค้นหา
        result = client.search(index=index_name, body=body)

    
        # return extract_data(result, options)
        return extract_data(result, options, geometry=geometry)


    except Exception as e:
        print(f"Error: {e}")
        return "An error occurred while processing the request."


def getBbox(index_name, minx, miny, maxx, maxy, resolution, options=None, geometry=False,output_file=None):

    # ตัวอย่างการเรียกใช้งาน
    host_input = os.getenv("host_insight")
    port_input = os.getenv("port_insight")

    # ตรวจสอบขนาดของ bbox ไม่เกิน 480,000 ตารางเมตร
    
        
    # ละติจูดที่ต้องการคำนวณ
    latitude = (miny + maxy) / 2

    # คำนวณ cosine ของละติจูด
    cos_latitude = math.cos(math.radians(latitude))

    # นำค่าไปใช้ในสูตรเพื่อแปลงเป็นตารางเมตร
    area_square_meters = (maxx - minx) * (maxy - miny) * 111 * cos_latitude
    print(area_square_meters)
    


    if options is None:
        # ไม่มีการส่ง options
        result_all_data = getmain(host_input, port_input, index_name, minx, miny, maxx, maxy, resolution, options=None, geometry=geometry)
        # print("Result without options:")
        # print(result_all_data)
        df = pd.DataFrame(result_all_data)
        # print(df)

        # print(df["geometry"])
        # json = df.to_json().encode('utf-8')
        # print(json)

    else:
        # มีการส่ง options มา
        result_selected_data = getmain(host_input, port_input, index_name, minx, miny, maxx, maxy, resolution, options=options, geometry=geometry)
        print("\nResult with specified options:")
        # print(result_selected_data)
        df = pd.DataFrame(result_selected_data)
        # print(df.columns)


    def cell_to_shapely(cell):
        coords = h3.h3_to_geo_boundary(cell)
        flipped = tuple(coord[::-1] for coord in coords)
        return Polygon(flipped)

    h3_geoms = df['h3_id'].apply(lambda x: cell_to_shapely(x))

    h3_gdf = gpd.GeoDataFrame(data=df, geometry=h3_geoms, crs=4326)

    # ตรวจสอบว่ามีการระบุชื่อไฟล์หรือไม่
    if output_file:
        h3_gdf.to_file(f"{output_file}.geojson", driver='GeoJSON')
        print(f"GeoJSON file '{output_file}.geojson' created successfully.")

