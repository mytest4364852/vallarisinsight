from opensearchpy import OpenSearch
import json
import os
from dotenv import load_dotenv


env_path = 'config_insight/.env'
load_dotenv(env_path)


def getAllProperties(host_input, port_input, user_input, password_input, index_name):
    # เชื่อมต่อ OpenSearch
    client = OpenSearch(
        hosts=[{"host": str(host_input), "port": int(port_input)}],
        http_auth=(user_input, password_input),
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    try:
        # ดึงข้อมูล mapping หรือ properties ของ index ที่ระบุ
        index_mapping = client.indices.get_mapping(index=index_name)

        # Extract and print properties
        properties = index_mapping[index_name]["mappings"]["properties"]
        json_string = []
        for field_name, field_info in properties.items():
            if field_name not in ['wkt', 'datadatetime', 'location']:
                json_out = {
                    "column": field_name,
                    "datatype": field_info['type']
                }
                json_string.append(json_out)

        return json_string  # ส่งกลับเป็น JSON string

    except Exception as e:
        print(f"Error: {e}")
        print(f"Index '{index_name}' not found or error retrieving properties.")  # แสดงข้อความแจ้งเตือน
        return None

def getProperties(index_name):
    host_input = os.getenv("host_insight")
    port_input = os.getenv("port_insight")
    user_input = os.getenv("user_insight")
    password_input = os.getenv("password_insight")

    if user_input is not None and password_input is not None:
        # เรียกใช้ฟังก์ชัน getAllProperties() เพื่อดึง properties ของ index ที่กำหนด
        all_properties = getAllProperties(host_input, port_input, user_input, password_input, index_name)

        if all_properties:
            print(f"All available properties for index '{index_name}':")
            print(all_properties)
        else:
            print("Failed to retrieve properties.")
    else:
        print("Please login.")


