from opensearchpy import OpenSearch
import h3
from shapely.geometry import Polygon
import os
from dotenv import load_dotenv

# โหลดตัวแปรสภาพแวดล้อมจากไฟล์ .env ที่ระบุเส้นทาง
env_path = 'config_insight/.env'
load_dotenv(env_path)


def create_open_search_body(minx, miny, maxx, maxy, resolution, options=None):
    
    # สร้างโครงสร้างของคำสั่งค้นหาใน OpenSearch
    body = {
        "aggregations": {
            "filter_agg": {
                "aggregations": {
                    "1": {
                        "aggs": {},
                        "geohex_grid": {
                            "field": "location",
                            "precision": resolution
                        }
                    }
                },
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "geo_bounding_box": {
                                    "location": {
                                        "bottom_right": {"lat": miny, "lon": minx},
                                        "top_left": {"lat": maxy, "lon": maxx}
                                    }
                                }
                            },
                            {
                                "range": {
                                    "datadatetime": {
                                        "gte": "0001-01-01T00:00:00Z",
                                        "lte": "2024-08-28T04:46:46.423753Z"
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        },
        "size": 0
    }


    # สร้างส่วน aggregation ของคำสั่งค้นหาจาก options
    if options:
        for option in options:
            body['aggregations']['filter_agg']['aggregations']["1"]["aggs"][f"{option}:majority"] = {
                "terms": {"field": f"{option}.keyword"}
            }

    # เพิ่มส่วนนี้เพื่อกรณีที่ไม่มี options
    else:
     
        # Include all columns (lat, lon, and hexId)
        for key in body:  # เพิ่มคอลัมน์ที่ต้องการทั้งหมด
            body['aggregations']['filter_agg']['aggregations']["1"]["aggs"][f"{key}:majority"] = {
                "terms": {"field": f"{key}.keyword"}
            }

    # print(body)
    return body

   


def extract_data(response, options):
    # ตรวจสอบว่ามีการตอบกลับ (response) หรือไม่
    if not response:
        return "No response"
    
    # กรองข้อมูลสำหรับ H3 index ที่ระบุ
    data = response['aggregations']['filter_agg']["1"]["buckets"]

     # หากไม่มีข้อมูล
    if not data:
        print("No data available.")
        return "This data is not available "

    # เตรียมข้อมูลสำหรับการคืนค่า
    filtered_data = data[0]
    
    # ถ้ามี options ถูกส่งมา
    if options:
        # สร้างลิสต์ของดิกชันนารี ({key: value, ...}) สำหรับแต่ละ key ใน options
        response = [{key: filtered_data[f"{key}:majority"]['buckets'][0]['key']} for key in options]
    else:
        # หากไม่มี options ถูกส่งมา (else:), สร้างดิกชันนารี ({key: value, ...}) โดยนำค่าทั้งหมดใน filtered_data มาใส่ในดิกชันนารี
        response = {key: filtered_data[key] for key in filtered_data.keys()}
    
    return response


def getAllProperties(host_input, port_input ,index_name):
    try:

        host_input = os.getenv("host_insight")
        port_input = os.getenv("port_insight")
        user_input = os.getenv("user_insight")
        password_input = os.getenv("password_insight")


        # เชื่อมต่อ OpenSearch
        client = OpenSearch(
            # hosts=[{"host": str(host) , "port": int(port)}],
            hosts=[{"host": str(host_input), "port": int(port_input)}],
            http_auth=(user_input, password_input),
            use_ssl=True,
            verify_certs=False,
            ssl_assert_hostname=False,
            ssl_show_warn=False,
        )

        # ดึงข้อมูล mapping หรือ properties ของ index ที่ระบุ
        index_mapping = client.indices.get_mapping(index=index_name)

        # Extract and print properties
        properties = index_mapping[index_name]["mappings"]["properties"]
        arr = []
        for field_name, field_info in properties.items():
            if field_name not in ['wkt', 'datadatetime', 'location']:
                json_out = {
                    "column": field_name,
                    "datatype": field_info['type']
                }
                arr.append(field_name)

        return arr  # ส่งกลับเป็น JSON string

    except Exception as e:
        print(f"Error: {e}")
        print(f"Index '{index_name}' not found or error retrieving properties.")
        return None




def getmain(host_input, port_input, index_name, latitude, longitude, resolution, options=None):

   
    host_input = os.getenv("host_insight")
    port_input = os.getenv("port_insight")
    user_input = os.getenv("user_insight")
    password_input = os.getenv("password_insight")
    
    # เชื่อมต่อ OpenSearch
    client = OpenSearch(
        hosts=[{"host":  str(host_input), "port": int(port_input)}],
        http_auth=(user_input, password_input),
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    if options is None:
        options = getAllProperties(host_input, port_input, index_name)

    else :
        pass

    try:
        # แปลงจุดพิกัดเป็น hexagon และ hexId
        h3_index = h3.geo_to_h3(latitude, longitude, int(resolution) + 1)
        print("H3 Index:", h3_index)

        # Get the vertices of the H3 cell
        vertices = h3.h3_to_geo_boundary(h3_index)

        # Create a Polygon from the vertices
        polygon = Polygon(vertices)         # สร้าง polygon ประกอบด้วยจุด hexagon 

        # Get bbox
        bbox = polygon.bounds      # ดึงขอบเขต polygon ที่ได้จะได้เป็นข้อมูล bbox 
        miny, minx, maxy, maxx = bbox

        # สร้างคำสั่งค้นหาใน OpenSearch
        body = create_open_search_body(minx, miny, maxx, maxy, resolution, options)

        # ดำเนินการค้นหา
        result = client.search(index=index_name, body=body)
       
        # print(result)

        h3_input = h3.geo_to_h3(latitude, longitude, int(resolution))
        print("H3 input:", h3_input)

        return extract_data(result, options)


    except Exception as e:
        print(f"Error: {e}")
        return "An error occurred while processing the request."


def getData(index_name, latitude, longitude, resolution, options=None):

    # ตัวอย่างการเรียกใช้งาน
    host_input = os.getenv("host_insight")
    port_input = os.getenv("port_insight")

    if options is None:
        # ไม่มีการส่ง options
        result_all_data = getmain(host_input, port_input, index_name, latitude, longitude, resolution)
        print("Result without options:")
        print(result_all_data)
    else:
        # มีการส่ง options มา
        result_selected_data = getmain(host_input, port_input, index_name, latitude, longitude, resolution, options=options)
        print("\nResult with specified options:")
        print(result_selected_data)

