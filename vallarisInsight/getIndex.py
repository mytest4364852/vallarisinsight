from opensearchpy import OpenSearch
import json
import os
from dotenv import load_dotenv

# โหลดตัวแปรสภาพแวดล้อมจากไฟล์ .env ที่ระบุเส้นทาง
env_path = 'config_insight/.env'
load_dotenv(env_path)

def getAllIndices(host_input, port_input, user_input, password_input):
    # เชื่อมต่อกับ OpenSearch
    client = OpenSearch(
        hosts=[{"host": str(host_input), "port": int(port_input)}],
        http_auth=(user_input, password_input),
        use_ssl=True,
        verify_certs=False,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    try:
        # ดึงข้อมูลทุกชื่อ indices จาก OpenSearch
        indices = client.indices.get(index='*')

        # กรองชื่อ indices ที่มีคำว่า "security" ออก
        indices = [index for index in indices.keys() if "security" not in index]

        # แปลง properties keys เป็น JSON string
        json_string = json.dumps(indices, indent=4)       # indent=4 เป็นการกำหนดให้เว้นวรรคใน json string

        return json_string
    except Exception as e:
        print(f"เกิดข้อผิดพลาด: {e}")
        return None

def getIndex():
    # ตรวจสอบว่าตัวแปร host และ port ได้ถูกตั้งค่าใน environment variables หรือไม่
    host_input = os.getenv("host_insight")
    port_input = os.getenv("port_insight")

    # ตรวจสอบว่าตัวแปร user และ password ได้ถูกตั้งค่าใน environment variables หรือไม่
    user_input = os.getenv("user_insight")
    password_input = os.getenv("password_insight")

    if user_input is not None and password_input is not None:
        # เรียกใช้ฟังก์ชัน getAllIndices() เพื่อดึงชื่อ indices ทั้งหมด
        all_indices = getAllIndices(host_input, port_input, user_input, password_input)

        if all_indices:
            print("All indices available:")
            
            return all_indices
        else:
            print("Unable to retrieve indices data.")
    else:
        print("Please login.")
